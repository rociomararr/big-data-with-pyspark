# Big Data with PySpark

Datasets and Notebooks with examples of how to work with PySpark, performing data cleaning, transforming, exploring, and applying Machine Learning and other algorithms to it.